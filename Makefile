SHELL = /bin/sh

#MAKRA
OBJS = tree.o solution.o
DOBJS = debug/tree.o debug/solution.o
CC = cc
CFLAGS = -std=c99
DFLAGS = -g
NAME = solution

#KOMPILACJA
.PHONY: all
all: solution

solution: $(OBJS)
	@$(CC) $(CFLAGS) -O3 -o $(NAME) $^

tree.o: tree.c
	@$(CC) $(CFLAGS) -c tree.c

solution.o: tree.h solution.c
	@$(CC) $(CFLAGS) -c solution.c

#DEBUG
.PHONY: debug

debug: $(DOBJS) 
	@$(CC) $(CFLAGS) $(DFLAGS) -o $(NAME).dbg $^
	
debug/tree.o: tree.c
	@mkdir -p debug
	@$(CC) $(CFLAGS) $(DFLAGS) -c tree.c
	@mv -u tree.o debug

debug/solution.o: solution.c
	@mkdir -p debug
	@$(CC) $(CFLAGS) $(DFLAGS) -c solution.c
	@mv -u solution.o debug

#CZYSZCZENIE
.PHONY: clean
clean: solution
	@rm -f *.o
	@rm -rf debug
