#!/bin/bash

COUNTER=0

liczbaParametrow=$#

if [ $liczbaParametrow -eq 2 ]
then
  prog=$1
  dir=$2

  for plik in $dir/*in
  do
    nazwa=$(basename $plik .in)
    ./$prog < $plik > $dir/${nazwa}b.out
    diff=$(diff $dir/$nazwa.out $dir/${nazwa}b.out)
    if [ "$diff" != "" ] 
    then 
      echo "test failed in $nazwa"
    else
        rm $dir/${nazwa}b.out
    fi
  done
elif [ $liczbaParametrow -gt 2 ]
then
  param=$1
  prog=$2
  dir=$3

  for plik in $dir/*in
  do
    nazwa=$(basename $plik .in)
    ./$prog $param < $plik > $dir/${nazwa}b.out 2> $dir/${nazwa}b.err

    diff=$(diff $dir/$nazwa.out $dir/${nazwa}b.out)
    if [ "$diff" != "" ] 
    then 
      echo "test failed in $nazwa "
    else
      rm $dir/${nazwa}b.out
    fi

    differr=$(diff $dir/$nazwa.err $dir/${nazwa}b.err)
    if [ "$differr" != "" ]
    then
      echo "diagnostic error in $nazwa"
    else
      rm $dir/${nazwa}b.err
    fi
  done
fi
