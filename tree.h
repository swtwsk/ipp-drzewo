#ifndef TREE_H_
#define TREE_H_

#include <stdlib.h>

//Counter for diagnostic purposes
long int globalActiveCounter;

//Tree root initialization and termination
void initTree();
void terminateTree();

//Operations on tree
void addNode(long int k);
long int rightmostChild(long int k);
void deleteNode(long int k);
void deleteSubtree(long int k);
void splitNode(long int k, long int w);

#endif