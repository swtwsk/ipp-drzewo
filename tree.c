#include "tree.h"

#define MAXN 500001
#define ROOT 0
#define NODEALLOC (Node *) malloc(sizeof(Node))
#define LISTALLOC (List *) malloc(sizeof(List))

typedef struct List List;
typedef struct Node Node;
typedef struct Node* Tree;

struct List
{
	long int val;
	struct List *next;
	struct List *previous;
	struct Node *node; //pointer to the node in tree
};

struct Node
{
	struct List *start; //beginning of list of childs
	struct List *end; //end of list of childs
	struct List *listElement; //pointer to itself on parent list
	struct Node *parentPointer; //pointer to parent node
};

long int globalNodeCounter = 1; //counter of all added nodes
long int globalActiveCounter = 1; //counter of nodes, reduced by deleted ones
Tree tree[MAXN]; //array of pointers to nodes

//Tree root initialization and termination
void initTree()
{
	Node *newNode = NODEALLOC;

	newNode->start = NULL;
	newNode->end = NULL;
	newNode->parentPointer = NULL;
	newNode->listElement = NULL;

	tree[ROOT] = newNode;
}

void terminateTree()
{
	deleteSubtree(ROOT);
	free(tree[ROOT]);
}

//List modifiers
void listPush(List **listEndPointer, Node *node, long int value)
{
	List *newElement = LISTALLOC;

	newElement->val = value;
	newElement->next = NULL;
	newElement->previous = *listEndPointer;
	newElement->node = node;

	if (*listEndPointer != NULL)
	{
		(*listEndPointer)->next = newElement;
	}

	*listEndPointer = newElement;

	node->listElement = newElement;
}

long int listBack(List *listEndPointer)
{
	if (listEndPointer == NULL)
	{
		return -1;
	}
	else
	{
		return listEndPointer->val;
	}
}

void listPop(List **elementToDelete)
{
	List *nextElement = (*elementToDelete)->next;
	List *prevElement = (*elementToDelete)->previous;

	if (prevElement != NULL)
	{
		prevElement->next = nextElement;
	}
	if (nextElement != NULL)
	{
		nextElement->previous = prevElement;
	}

	free(*elementToDelete);
}

void listReplace(List **elementToReplace, List **firstToInsert, List **lastToInsert)
{
	List *nextElement = (*elementToReplace)->next;
	List *prevElement = (*elementToReplace)->previous;

	if (prevElement != NULL)
	{
		prevElement->next = *firstToInsert;
	}
	if (nextElement != NULL)
	{
		nextElement->previous = *lastToInsert;
	}

	(*firstToInsert)->previous = prevElement;
	(*lastToInsert)->next = nextElement;

	free(*elementToReplace);
}

List *listSwapBack(List **frontElement, long int value)
{
	List *newElement = LISTALLOC;

	newElement->val = value;
	newElement->next = NULL;
	newElement->previous = *frontElement;
	newElement->node = NULL; //NULL until initialized in concrete function

	List *oldNextElement = (*frontElement)->next;
	(*frontElement)->next = newElement;

	return oldNextElement;
}

//ADD_NODE
void addNode(long int k)
{
	Node *newNode = NODEALLOC;

	newNode->start = NULL;
	newNode->end = NULL;
	newNode->parentPointer = tree[k];

	listPush(&(tree[k]->end), newNode, globalNodeCounter);

	if (tree[k]->start == NULL)
	{
		tree[k]->start = tree[k]->end;
	}

	tree[globalNodeCounter] = newNode;

	globalNodeCounter++;
	globalActiveCounter++;
}

//RIGHTMOST_CHILD
long int rightmostChild(long int k)
{
	return listBack(tree[k]->end);
}

//DELETE_NODE
void deleteNodeWithoutSons(List **parentPointerToModify, Node **neighbour, long int k)
{
	listPop(&tree[k]->listElement);

	(*neighbour)->parentPointer = tree[k]->parentPointer;
	*parentPointerToModify = (*neighbour)->listElement;
}

void deleteNodeWithSons(List **parentPointerToModify, Node **son, long int k)
{
	listReplace(&tree[k]->listElement, &tree[k]->start, &tree[k]->end);

	(*son)->parentPointer = tree[k]->parentPointer;
	*parentPointerToModify = (*son)->listElement;
}

void deleteLeftmostNode(long int k)
{
	if (tree[k]->start == NULL)
	{
		if (tree[k]->listElement->next != NULL)
		{
			Node *neighbour = tree[k]->listElement->next->node;

			deleteNodeWithoutSons(&tree[k]->parentPointer->start, &neighbour, k); //??
		}
		else
		{
			listPop(&tree[k]->listElement);

			tree[k]->parentPointer->start = NULL;
			tree[k]->parentPointer->end = NULL;
		}
	}
	else
	{
		if (tree[k]->listElement->next != NULL)
		{
			Node *son = tree[k]->start->node;

			deleteNodeWithSons(&tree[k]->parentPointer->start, &son, k);
		}
		else
		{
			List *leftSon = tree[k]->start;
			List *rightSon = tree[k]->end;

			listReplace(&tree[k]->listElement, &tree[k]->start, &tree[k]->end);

			leftSon->node->parentPointer = tree[k]->parentPointer;
			rightSon->node->parentPointer = tree[k]->parentPointer;

			tree[k]->parentPointer->start = leftSon;
			tree[k]->parentPointer->end = rightSon;
		}
	}
}

void deleteRightmostNode(long int k)
{
	if (tree[k]->start == NULL)
	{
		Node *neighbour = tree[k]->listElement->previous->node;

		deleteNodeWithoutSons(&tree[k]->parentPointer->end, &neighbour, k);
	}
	else
	{
		Node *son = tree[k]->end->node;

		deleteNodeWithSons(&tree[k]->parentPointer->end, &son, k);
	}
}

void deleteInBetweenNode(long int k)
{
	if (tree[k]->start == NULL)
	{
		listPop(&tree[k]->listElement);
	}
	else
	{
		listReplace(&tree[k]->listElement, &tree[k]->start, &tree[k]->end);
	}
}

void deleteNode(long int k)
{
	globalActiveCounter--;

	if (tree[k]->listElement->previous == NULL)
	{
		deleteLeftmostNode(k);
	}
	else if (tree[k]->listElement->next == NULL)
	{
		deleteRightmostNode(k);
	}
	else
	{
		deleteInBetweenNode(k);
	}

	free(tree[k]);
	tree[k] = NULL;
}

//DELETE_SUBTREE
void deleteSubtree(long int k)
{
	while (tree[k]->start != NULL)
	{
		deleteNode(tree[k]->start->val);
	}
	
	if (k > 0)
	{
		deleteNode(k);
	}
}

//SPLIT_NODE
void splitNode(long int k, long int w)
{
	Node *parentNode = tree[k];
	Node *activeVertex = tree[w];

	if (activeVertex->listElement == parentNode->end)
	{
		addNode(k);
	}
	else
	{
		List *NextElement = listSwapBack(&activeVertex->listElement, globalNodeCounter);
		List *LastElement = parentNode->end;

		parentNode->end = activeVertex->listElement->next;

		Node *newNode = NODEALLOC;

		newNode->start = NextElement;
		newNode->end = LastElement;
		newNode->parentPointer = parentNode;
		newNode->listElement = parentNode->end;

		newNode->listElement->node = newNode;
		tree[globalNodeCounter] = newNode;

		NextElement->node->parentPointer = newNode;
		LastElement->node->parentPointer = newNode;
		
		NextElement->previous = NULL;

		globalActiveCounter++;
		globalNodeCounter++;
	}
}