#include <stdio.h>
#include <string.h>
#include "tree.h"

int initParametr(int argc, char *argv[])
{
	int errorHandling = 0;

	int i;
	for (i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-v") == 0)
		{
			errorHandling = 1;
		}
		else
		{
			return -1;
		}
	}

	if (errorHandling == 1)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void readFromInput(char order[20])
{
	long int k;

	if (strcmp(order, "ADD_NODE") == 0)
	{
		scanf("%ld", &k);
		addNode(k);
		printf("OK\n");
	}
	else if (strcmp(order, "RIGHTMOST_CHILD") == 0)
	{
		scanf("%ld", &k);
		printf("%ld\n", rightmostChild(k));
	}
	else if (strcmp(order, "DELETE_NODE") == 0)
	{
		scanf("%ld", &k);
		deleteNode(k);
		printf("OK\n");
	}
	else if (strcmp(order, "DELETE_SUBTREE") == 0)
	{
		scanf("%ld", &k);
		deleteSubtree(k);
		printf("OK\n");
	}
}

int main(int argc, char *argv[])
{
	long int k, w;
	char order[20];

	int errorHandling = initParametr(argc, argv);
	if (errorHandling < 0)
	{
		printf("ERROR\n");
		exit(1);
	}

	initTree();

	while (scanf("%19s", order) == 1)
	{
		if (strcmp(order, "SPLIT_NODE") == 0)
		{
			scanf("%ld %ld", &k, &w);
			splitNode(k, w);
			printf("OK\n");
		}
		else
		{
			readFromInput(order);
		}

		if (errorHandling == 1)
		{
			fprintf(stderr, "NODES: %ld\n", globalActiveCounter);
		}
	}

	terminateTree();

	return 0;
}
